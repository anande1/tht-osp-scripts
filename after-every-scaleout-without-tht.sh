# Name: after_every_scaleout_operation.sh
# Description: This script should be executed after every scale out operation
# Check the greyed out / commented sections for any changes that need to be done

source /home/stack/stackrc

## This will copy the configure_cinder_backend.sh script to every controller old/new
## and execute it to undo the over-written cinder.conf changes on the controller nodes.
for i in ` nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do scp /home/stack/scripts_used/configure_cinder_backends.sh heat-admin@$i:/home/heat-admin/; done
for i in ` nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo sh /home/heat-admin/configure_cinder_backends.sh';done

## This will restore the ml2_conf.ini on all controllers
for i in ` nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/neutron/plugins/ml2/ml2_conf.ini ml2 network_vlan_ranges datacentre:132:135';done
for i in ` nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/neutron/plugins/ml2/ml2_conf.ini ml2 tenant_network_types vxlan,vlan';done

## (Keeping this commented for now - as every provider network has its own default gateway)
## This will restore dhcp_metadata on all computes + controllers
# for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/neutron/dhcp_agent.ini DEFAULT enable_isolated_metadata True';done
# ssh heat-admin@172.19.x.x 'sudo pcs resource restart neutron-dhcp-agent'
# ssh heat-admin@172.19.x.x 'sudo pcs resource restart neutron-metadata-agent'

## This will iteratively copy the Navicli rpm tp all the compute nodes old/new in /tmp dir
## and confirm + install the rpm on the compute nodes.
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do scp NaviCLI-Linux-64-x86-en_US-7.33.9.1.55-1.x86_64.rpm heat-admin@$i:/tmp/NaviCLI-Linux-64-x86-en_US-7.33.9.1.55-1.x86_64.rpm; done
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo ls /tmp/Navi*';done
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo yum localinstall -y /tmp/NaviCLI-Linux-64-x86-en_US-7.33.9.1.55-1.x86_64.rpm';done
# (the following iscsi_use_multipath options is valid for both iscsi+FC as protocol)
# (reference http://docs.openstack.org/liberty/config-reference/content/emc-vnx-driver.html#multipath-setup)
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/nova/nova.conf libvirt iscsi_use_multipath True';done
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/nova/nova.conf DEFAULT block_device_allocate_retries 120';done
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/nova/nova.conf DEFAULT block_device_creation_timeout 300';done
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo openstack-config --set /etc/nova/nova.conf DEFAULT block_device_allocate_retries_interval 10';done

# [only if there is instance-HA configured] ssh heat-admin@$172.19.x.x 'sudo pcs resource restart openstack-nova-compute.service'
for i in ` nova list  | grep comp | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl restart openstack-nova-compute.service';done
## Enable repo and install multipath related packages on controller+computes
### If you want to register the overcloud nodes again to subscription-manager,
### uncomment the following ONE line only and pass the relevant credentials :
#for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo subscription-manager register --username <username> --password <pass> --auto-attach'; done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo subscription-manager repos --enable  rhel-7-server-rpms'; done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo yum -y install sg3_utils sysfsutils device-mapper-multipath'; done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo mpathconf --enable --with_multipathd y';done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo mv /etc/multipath.conf /etc/multipath.conf.old';done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do scp /home/stack/scripts_used/multipath.conf heat-admin@$i:/home/heat-admin/multipath.conf; done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo mv /home/heat-admin/multipath.conf /etc/multipath.conf';done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo chown root:root /etc/multipath.conf && sudo chmod 0600 /etc/multipath.conf'; done
for i in ` nova list  | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl reload multipathd.service';done


# Finally restart the pcs resources for the respective services:
ssh heat-admin@172.19.x.x 'sudo pcs resource restart openstack-cinder-volume'
# And cleanup any failed resources
# ssh heat-admin@172.19.x.x "for i in `sudo pcs status|grep -B2 Stop |grep -v "Stop\|Start"|awk -F"[" '/\[/ {print substr($NF,0,length($NF)-1)}'`; do echo $i; sudo pcs resource cleanup $i; done"
