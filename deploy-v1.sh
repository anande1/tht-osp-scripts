time openstack overcloud deploy \
--templates /home/stack/templates/openstack-tripleo-heat-templates \
-e /home/stack/templates/openstack-tripleo-heat-templates/environments/network-environment.yaml \
-e /home/stack/templates/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
-e /home/stack/templates/nic-mapping.yaml \
--control-scale 3 \
--compute-scale 12 \
--ceph-storage-scale 0 \
--compute-flavor compute \
--control-flavor control \
--neutron-bridge-mappings provider:br-provider \
--neutron-network-vlan-ranges provider:231:4055 \
--force-postconfig \
--ntp-server 172.16.13.98 \
--debug \
--validation-errors-fatal
