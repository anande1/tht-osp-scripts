#!/bin/bash
if [[ `hostname` = *"XXXXX"* ]]
then
  echo "Number of disks detected: $(lsblk -no NAME,TYPE,MOUNTPOINT | grep "disk" | awk '{print $1}' | wc -l)"
  for DEVICE in `lsblk -no NAME,TYPE,MOUNTPOINT | grep "disk" | awk '{print $1}'`
  do
    ROOTFOUND=0
    echo "Checking /dev/$DEVICE..."
    echo "Number of partitions on /dev/$DEVICE: $(expr $(lsblk -n /dev/$DEVICE | awk '{print $7}' | wc -l) - 1)"
    for MOUNTS in `lsblk -n /dev/$DEVICE | awk '{print $7}'`
    do
      if [ "$MOUNTS" = "/" ]
      then
        ROOTFOUND=1
      fi
    done
    if [ $ROOTFOUND = 0 ]
    then
      echo "Root not found in /dev/${DEVICE}"
      echo "Wiping disk /dev/${DEVICE}"
      sgdisk -Z /dev/${DEVICE}
      EMPH_DISKS=$EMPH_DISKS" /dev/${DEVICE}"
    else
      echo "Root found in /dev/${DEVICE}"
    fi
  done
pvcreate $EMPH_DISKS
vgcreate XYZ_vg $EMPH_DISKS
lvcreate -l 100%FREE -n XYZ_lv XYZ_vg
mkfs.xfs -f /dev/mapper/XYZ_vg-XYZ_lv
if [ ! -d /var/lib/nova/instances ] ; then mkdir -p /var/lib/nova/instances ; fi
#Avoid fsck . In case of corruption, admin has to manually recover in maintenance mode if mount fails .
echo "/dev/mapper/XYZ_vg-XYZ_lv /var/lib/nova/instances                   xfs     defaults        0 0" >> /etc/fstab
mount -av
chown -R nova:nova /var/lib/nova/instances
restorecon -rv /var/lib/nova/instances
fi
