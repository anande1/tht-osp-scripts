## Deploy script "without" storage-environment.yaml file,
## Just moves the /dev/sdb to volume-group : cinder-volumes
## and mounts /var/lib/nova/instances on it.
## Creates provider networks.

time openstack overcloud deploy \
--templates /home/stack/templates/openstack-tripleo-heat-templates \
-e /home/stack/templates/openstack-tripleo-heat-templates/environments/network-environment.yaml \
-e /home/stack/templates/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
-e /home/stack/templates/nic-mapping.yaml \
-e /home/stack/templates/openstack-tripleo-heat-templates/firstboot/ephemeral-approach-3/compute-post-env.yaml \
--control-scale 1 \
--compute-scale 1 \
--ceph-storage-scale 0 \
--compute-flavor compute \
--control-flavor control \
--neutron-bridge-mappings provider:br-provider \
--neutron-network-vlan-ranges provider:231:4055 \
--ntp-server 172.16.13.98 \
--debug \
--validation-errors-fatal
