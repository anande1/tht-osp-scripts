## To be added in ExtraConfig section in firstboot.yaml

fdisk /dev/sdb 
n
p
1
select default first-sector
select default last-sector
t
8e
w
mkfs.xfs /dev/sdb1 -f
echo -e "/dev/sdb1\t/var/lib/nova/instances\txfs\tdefaults\t0 0" >> /etc/fstab
pvcreate /dev/sdb1
vgcreate nova-volumes /dev/sdb1
lvcreate -l 100%FREE -n nova-lv nova-volumes
mount /dev/nova-volumes/nova-lv /var/lib/nova/instances/
chown -R nova. /var/lib/nova/instances/
restorecon -rv /var/lib/nova/instances/
