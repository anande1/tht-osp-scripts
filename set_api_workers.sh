source /home/stack/stackrc

for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/glance/glance-registry.conf DEFAULT workers 1';done
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl restart openstack-glance-registry.service';done

for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/glance/glance-api.conf DEFAULT workers 1';done
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl restart openstack-glance-api.service';done

for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/neutron/metadata_agent.ini DEFAULT metadata_workers 1';done 
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl restart neutron-metadata-agent.service';done

for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/neutron/neutron.conf DEFAULT api_workers 1';done 
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/neutron/neutron.conf DEFAULT rpc_workers 1';done 
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl restart neutron-server.service';done

for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/keystone/keystone.conf eventlet_server public_workers 1';done 
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo crudini --set /etc/keystone/keystone.conf eventlet_server admin_workers 1';done 
for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'sudo systemctl restart httpd.service';done

for i in `nova list  | grep control | awk '{print $12}' |  cut -f2 -d "="`; do ssh heat-admin@$i 'echo -e "\n======";hostname;echo -e "=======";echo ":KeystoneWorkers::::";sudo egrep -i "public_workers|admin_workers" /etc/keystone/keystone.conf | grep -v ^#;echo ":NeutronServer::::";sudo egrep -i "api_workers|rpc_workers" /etc/neutron/neutron.conf | grep -v ^#;echo ":GlanceRegistry::::";sudo grep workers /etc/glance/glance-registry.conf | grep -v ^#;echo ":GlanceApi::::";sudo grep workers /etc/glance/glance-api.conf | grep -v ^#;echo ":NeutronMetaDataAgent::::";sudo grep workers /etc/neutron/metadata_agent.ini | grep -v ^#';done
